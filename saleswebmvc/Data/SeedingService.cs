﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using saleswebmvc.Models;
using saleswebmvc.Models.Enums;

namespace saleswebmvc.Data
{
    public class SeedingService
    {
        private SalesWebMvcContext _context;

        public SeedingService(SalesWebMvcContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            if (_context.Department.Any() || 
                _context.Seller.Any() ||
                _context.SalesRecord.Any())
            {
                return; // DB has been seeded
            }

            Department d1 = new Department(1, "Computers");
            Department d2 = new Department(2, "Eletronics");
            Department d3 = new Department(3, "Fashion");
            Department d4 = new Department(4, "Books");

            Seller s1 = new Seller(1, "Bob Marley", "bobmarley@gmail.com", new DateTime(1993, 4, 28), 10000.0, d1);
            Seller s2 = new Seller(2, "Bob Lee", "boblee@gmail.com", new DateTime(1993, 4, 22), 9000.0, d2);
            Seller s3 = new Seller(3, "Bob Swag", "bobswag@gmail.com", new DateTime(1999, 4, 12), 9500.0, d1);
            Seller s4 = new Seller(4, "Bob Tesh", "bobtesh@gmail.com", new DateTime(1991, 4, 15),5000.0, d3);
            Seller s5 = new Seller(5, "Bob Rush", "bobrush@gmail.com", new DateTime(1990, 4, 15),5000.0, d4);

            SalesRecord r1 = new SalesRecord(1, new DateTime(2018, 09, 25), 15000.0, SaleStatus.Billed, s1);
            SalesRecord r2 = new SalesRecord(2, new DateTime(2018, 06, 25), 200.0, SaleStatus.Billed, s2);
            SalesRecord r3 = new SalesRecord(3, new DateTime(2018, 06, 25), 590.0, SaleStatus.Billed, s3);
            SalesRecord r4 = new SalesRecord(4, new DateTime(2018, 02, 25), 350.0, SaleStatus.Billed, s1);
            SalesRecord r5 = new SalesRecord(5, new DateTime(2018, 09, 25), 6500.0, SaleStatus.Billed, s3);
            SalesRecord r6 = new SalesRecord(6, new DateTime(2018, 03, 25), 980.0, SaleStatus.Billed, s2);
            SalesRecord r7 = new SalesRecord(7, new DateTime(2018, 09, 25), 350.0, SaleStatus.Billed, s1);
            SalesRecord r8 = new SalesRecord(8, new DateTime(2018, 09, 25), 75000.0, SaleStatus.Billed, s4);
            SalesRecord r9 = new SalesRecord(9, new DateTime(2018, 03, 25), 150.0, SaleStatus.Billed, s1);
            SalesRecord r10 = new SalesRecord(10, new DateTime(2018, 09, 25), 350.0, SaleStatus.Billed, s3);

            _context.Department.AddRange(d1, d2, d3, d4);
            _context.Seller.AddRange(s1, s2, s3, s4, s5);
            _context.SalesRecord.AddRange(
                r1, r2, r3, r4, r5,
                r6, r7, r8, r9, r10
            );

            _context.SaveChanges();
        }

    }
}
