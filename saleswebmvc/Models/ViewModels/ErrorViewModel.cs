using System;

namespace saleswebmvc.Models.ViewModels
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public string Message { get; set; } // Added

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}