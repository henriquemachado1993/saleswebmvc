﻿using saleswebmvc.Models.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace saleswebmvc.Models
{
    public class SalesRecord
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        [DataType(DataType.Date)] // Define semantics for date
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")] // Define format for date
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        [DisplayFormat(DataFormatString = "{0:F2}")] // Define format for coin
        public double Amount { get; set; }

        [Required(ErrorMessage = "{0} Required")]
        public SaleStatus Status { get; set; }

        // Association
        public Seller Seller { get; set; }

        [Display(Name = "Seller")]
        [Required(ErrorMessage = "{0} Required")]
        public int SellerId { get; set; }

        public SalesRecord()
        {

        }

        public SalesRecord(int id, DateTime date, double amount, SaleStatus status, Seller seller)
        {
            Id = id;
            Date = date;
            Amount = amount;
            Status = status;
            Seller = seller;
        }
    }
}
