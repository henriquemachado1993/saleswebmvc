﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace saleswebmvc.Migrations
{
    public partial class OtherEntities2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SalesRecord_SalesRecord_SellerId1",
                table: "SalesRecord");

            migrationBuilder.DropIndex(
                name: "IX_SalesRecord_SellerId1",
                table: "SalesRecord");

            migrationBuilder.DropColumn(
                name: "SellerId1",
                table: "SalesRecord");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SellerId1",
                table: "SalesRecord",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SalesRecord_SellerId1",
                table: "SalesRecord",
                column: "SellerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_SalesRecord_SalesRecord_SellerId1",
                table: "SalesRecord",
                column: "SellerId1",
                principalTable: "SalesRecord",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
